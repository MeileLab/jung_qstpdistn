#include "palabos2D.h"
#include "palabos2D.hh"
#include <vector>
#include <cmath>
#include <cstdlib>
#include <sys/stat.h>
#include <ctime>
#include <stack>
 
using namespace plb;
typedef double T;

#define NSDES descriptors::D2Q9Descriptor
#define RXNDES descriptors::AdvectionDiffusionD2Q5Descriptor

// a function to create the output storage folder if it doesn't exist already
bool fexists(const char *filename) {
    plb_ifstream checkfile(filename);
    if (checkfile.is_open()) {
        return 1;
        checkfile.close();
    }
    else {
        return 0;
    }
}

// a function to initially distribute the pressure linearly.
// This is used only to initializing the flow field (functional: NSdomainSetup)
class PressureGradient {
public:
    PressureGradient(T deltaP_, plint nx_) : deltaP(deltaP_), nx(nx_)
    { }
    void operator() (plint iX, plint iY, T& density, Array<T,2>& velocity) const
    {
        velocity.resetToZero();
        density = (T)1 - deltaP*NSDES<T>::invCs2 / (T)(nx-1) * (T)iX;
    }
private:
    T deltaP;
    plint nx;
};

// writing output files of the flow domain
void writeNsVTK(MultiBlockLattice2D<T,NSDES>& lattice, plint iter, std::string nameid)
{
    VtkImageOutput2D<T> vtkOut(createFileName("vtkVelocity" + nameid, iter, 7), 1.);

    vtkOut.writeData<float>(*computeVelocityNorm(lattice), "velocityNorm", 1.);
    vtkOut.writeData<2,float>(*computeVelocity(lattice), "velocity", 1.);
    vtkOut.writeData<float>(*computeDensity(lattice), "Density", 1.);
}

// Flow domain boundary conditions. Top and bottom boundaries are using bounce-back boundaries.
void NSdomainSetup(MultiBlockLattice2D<T,NSDES>& lattice,
                    OnLatticeBoundaryCondition2D<T,NSDES>& boundaryCondition, T deltaP)
{
    const plint nx = lattice.getNx();
    const plint ny = lattice.getNy();

    Box2D west   (0,0, 0,ny-1);
    Box2D east   (nx-1,nx-1, 0,ny-1);

    // Set the boundary-conditions
    boundaryCondition.addPressureBoundary0N(west, lattice); // 0N: negative (N) in x-direction (0) (e.g. 1P: positiv in y-direction)
    setBoundaryDensity(lattice, west, (T) 1.);

    boundaryCondition.addPressureBoundary0P(east, lattice); // 0P: positive(P) in x-direction (0)
    setBoundaryDensity(lattice, east, (T) 1. - deltaP*NSDES<T>::invCs2);

    initializeAtEquilibrium(lattice, lattice.getBoundingBox(), PressureGradient(deltaP, nx));
    lattice.initialize();
}

// solute domain boundary conditions. top and bottom are using bounce-back boundaries.
void concentrationSetup(MultiBlockLattice2D<T,RXNDES> &lattice,
                        OnLatticeAdvectionDiffusionBoundaryCondition2D<T, RXNDES> &BC,
                        plint nx, plint ny, T ux) {
 
    Box2D left(0, 0, 0, ny-1);
    Box2D right(nx-1 ,nx-1, 0, ny-1);
    plint processorLevelBC = 1;
 
    // Set the boundary-conditions
    BC.addTemperatureBoundary0N(left, lattice);
    setBoundaryDensity(lattice, left, 0.0); // fixed concentration

    BC.addTemperatureBoundary0P(right, lattice);
    integrateProcessingFunctional(new FlatAdiabaticBoundaryFunctional2D<T,RXNDES, 0, +1>, right, lattice, processorLevelBC); // no gradient
    // FlatAdiabaticBoundaryFunctional2D copies the population at "Box2D right" and pastes to the right"+1" location in x-direction ("0")
    // integrateProcessingFunctional is a functional being executed after collision and streaming steps.
    // processorLevelBC specifies the execution order of internal data processors. See the palabos manual for more details.

    // Init lattice
    Array<T,2> jEq(ux, 0);
    initializeAtEquilibrium(lattice, lattice.getBoundingBox(), (T) 0, jEq);
    lattice.initialize();
}

// writing output files of the solute domain
void writeAdvVTK(MultiBlockLattice2D<T,RXNDES>& lattice, plint iter, std::string nameid)
{
    VtkImageOutput2D<T> vtkOut(createFileName("vtk" + nameid, iter, 7), 1.);
    vtkOut.writeData<T>(*computeDensity(lattice), "Density", 1.);
}

/* ========== DATA PROCESSING FUNCTIONAL ========== */

// source term producing solutes at a fixed location (here, x = 500, y = 0)
template<typename T, template<typename U> class Descriptor>
class localReactionLattices2D : public BoxProcessingFunctional2D_L<T,Descriptor>
{
public:
    localReactionLattices2D(T k1_) : k1(k1_)
    {}
    virtual void process(Box2D domain, BlockLattice2D<T,Descriptor>& lattice) {
        Dot2D absoluteOffset = lattice.getLocation();
        for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
            plint absX = iX + absoluteOffset.x;
            for (plint iY=domain.y0; iY<=domain.y1; ++iY) {
                plint absY = iY + absoluteOffset.y;
                if (absX == 500 && absY == 0) {
                /*  1. ------------------------      2. -------------------------
                       | 1 2 3 4 5 6 7 8 9 10 | ===>    | 1 2 3 4 5 | 1 2 3 4 5 |
                       ------------------------         -------------------------
                    domain 1 is parallelized to domain 2. The data processor operates on the parallelized domain.
                    absX and absY are to recover the absolute coordiates of domain 1 from domain 2. */

                    Array<T,5> g;
                    T cdot = k1;
                    // uncomment the following lines to include autoinduction. For now, autoinduction is ignored.
                    // T c0 = lattice.get(iX,iY).computeDensity(); // (mM)
                    // if (c0>1) {
                    //     cdot *= 11;
                    // }
                    lattice.get(iX,iY).getPopulations(g);
                    g[0]+=(T) cdot/3; g[1]+=(T) cdot/6; g[2]+=(T) cdot/6; g[3]+=(T) cdot/6; g[4]+=(T) cdot/6;
                    lattice.get(iX,iY).setPopulations(g);
                }
            }
        }
    }
    virtual BlockDomain::DomainT appliesTo() const {
        return BlockDomain::bulk;
    }
    virtual localReactionLattices2D<T,Descriptor>* clone() const {
        return new localReactionLattices2D<T,Descriptor>(*this);
    }
    void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::staticVariables;
    }
private:
    T k1;
};

// =============================================== MAIN PROGRAM =============================================== //

int main(int argc, char **argv)
{
    plbInit(&argc, &argv);

    const T deltaP = atof(argv[1]);
    const T Pe = atof(argv[2]);
    const T Da = atof(argv[3]);
    const T nsLatticeTau = atof(argv[4]);

    // command line example
    // mpirun qsTpDistn 'deltaP' 'Pe' 'Da' '0.5<tau<2'

    struct stat statStruct;
    const char *dirname = "output/";
    
    srand ( unsigned ( std::time(0) ) );
 
    stat(dirname, &statStruct);
 
    if (S_ISDIR(statStruct.st_mode)) {
        pcout << "output folder already exists" << std::endl;
    }
    else {
        pcout << "Creating output folder (./output)" << std::endl;
        mkdir(dirname, 0777);
    }
    global::directories().setOutputDir(dirname);

    // Parameters for the dynamics
    const plint nx = 2002; // with 2 ghost nodes
    const plint ny = 1000;
    const plint lenc = (nx-2)/ny;
    const T nsLatticeOmega = 1 / nsLatticeTau;
    const T nsLatticeNu = NSDES<T>::cs2*(nsLatticeTau-0.5);
    pcout << "nsLatticeTau = " << nsLatticeTau <<", nsLatticeOmega = " << nsLatticeOmega << ", nsLatticeNu = " << nsLatticeNu << std::endl;

    // Instantiate the lattice
    MultiBlockLattice2D<T, NSDES> nsLattice( nx, ny, new IncBGKdynamics<T, NSDES>(nsLatticeOmega) );
    OnLatticeBoundaryCondition2D<T,NSDES> *nsBC = createLocalBoundaryCondition2D<T, NSDES>();
    NSdomainSetup(nsLattice, *nsBC, deltaP);
    util::ValueTracer<T> converge(1.0,1000.0,1.e-8);
    
    const char *NS1name = "./output/nsIntLattice1CheckPoint.dat";
    for (iT=0; iT < 10000000; ++iT) {
        nsLattice.collideAndStream();
        // check the convergence and break out of the loop
        converge.takeValue(getStoredAverageEnergy(nsLattice),true);
        if (converge.hasConverged()) {
            break;
        }
    }
    pcout << std::endl << "flow calc finished at iT = " << iT << std::endl;
    pcout << "Writing velocity VTK... \n";
    writeNsVTK(nsLattice,iT,"nsLatticeFinal");
    pcout << "Writing checkpoint... \n\n";
    saveBinaryBlock(nsLattice, NS1name);

    // compute the mean pore velocity
    T PoreMeanU = computeAverage(*computeVelocityNorm(nsLattice, Box2D (1,nx-2,0,ny-1)));

    // mean pore velocity is used to calculate nu (diffusion coefficient)
    T rxnLatticeNu = PoreMeanU * lenc / Pe;
    T rxnLatticeTau = rxnLatticeNu * RXNDES<T>::invCs2 + 0.5;
    T rxnLatticeOmega = 1/rxnLatticeTau;

    // calculate the rate constant (k1) from a predefined Da to use k1 as a model input
    T k1 = Da*rxnLatticeNu/lenc/lenc;
    T convThrs = 1e-10;
    T tmpSum0 = 0;
    pcout << "PoreMeanU = " << PoreMeanU << ", rxnLatticeTau = " << rxnLatticeTau << ", k1 = " << k1 << ", Da = " << Da << std::endl;
    T PoreMaxUx = computeMax(*computeVelocityComponent(nsLattice, Box2D (1,nx-2,0,ny-1), 0));
    // Mach number << 1 to ensure incompressible NS
    pcout << "Mach number = " << PoreMaxUx/sqrt(RXNDES<T>::cs2) << std::endl  << std::endl;

    // Istantiate adv-diff lattice
    MultiBlockLattice2D<T, RXNDES> advLattice( nx, ny, new AdvectionDiffusionRLBdynamics<T,RXNDES>(rxnLatticeOmega) );
    OnLatticeAdvectionDiffusionBoundaryCondition2D<T, RXNDES> *avBC = createLocalAdvectionDiffusionBoundaryCondition2D<T, RXNDES>();
    // Set-up concentration field
    concentrationSetup(advLattice, *avBC, nx, ny, 0.);
 
    // First attempt to couple the two physics
    latticeToPassiveAdvDiff(nsLattice, advLattice, advLattice.getBoundingBox());

    plint maxiT = 10000000;
    plint iT = 0;
    for (; iT <= maxiT; ++iT) {
        if(iT%10000==0) {
            T tmpSum1 = computeSum(*computeDensity(advLattice), advLattice.getBoundingBox());
            T RMSE = std::sqrt( (tmpSum0-tmpSum1)*(tmpSum0-tmpSum1)/nx/ny );
            if ( RMSE  < convThrs && iT > 0) {
                pcout << "Simulation has reached a steady state.\nStopping the ADE loop for the advLattice at the time step (iT = " << iT << ", convergence = " << RMSE << ").\n";
                break;
            }
            else {
                tmpSum0 = tmpSum1;
                pcout << "RMSE (iT = " << iT << ") = " << RMSE << std::endl;
            }
        }
        advLattice.collide();
        applyProcessingFunctional(new localReactionLattices2D<T,RXNDES> (k1), advLattice.getBoundingBox(), advLattice );
        advLattice.stream();
    }


    pcout << "Writing the output data... \n";
    std::string var1 = std::to_string(Pe);
    std::string var2 = std::to_string(Da);
    std::string str1 = createFileName("./output/Pe" + var1 + "Da" + var2 + ".out", 0, 1);
    char *cstr1 = new char[str1.length() + 1];
    strcpy(cstr1, str1.c_str());
    std::string str2 = createFileName("Pe" + var1 + "Da" + var2, 0, 1);
    char *cstr2 = new char[str2.length() + 1];
    strcpy(cstr2, str2.c_str());
    
    plb_ofstream ofileX(cstr1);
    ofileX << std::setprecision(9) << *computeDensity(advLattice) << std::endl;

    writeAdvVTK(advLattice, iT, cstr2);

    return 0;
}
